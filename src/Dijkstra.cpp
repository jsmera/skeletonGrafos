#include "graph.h"
#include "graphSearch.h"
#include <iostream>
#include <cstdio>

using namespace std;

int main() {
  OperacionesGrafo<int> op;
  
  //test 1 Dijkstra
  Nodo<int> nd0(0);
  Nodo<int> nd1(1);
  Nodo<int> nd2(2);
  Nodo<int> nd3(3);
  Nodo<int> nd4(4);
  Nodo<int> nd5(5);
  Nodo<int> nd6(6);
  Nodo<int> nd7(7);
  Nodo<int> nd8(8);

  GrafoPonderado<int> gTestD(false);

  gTestD.agregarNodo(nd0);
  gTestD.agregarNodo(nd1);
  gTestD.agregarNodo(nd2);
  gTestD.agregarNodo(nd3);
  gTestD.agregarNodo(nd4);
  gTestD.agregarNodo(nd5);
  gTestD.agregarNodo(nd6);
  gTestD.agregarNodo(nd7);
  gTestD.agregarNodo(nd8);

  gTestD.agregarArista(0, 1, 4);
  gTestD.agregarArista(0, 7, 8);
  gTestD.agregarArista(1, 2, 8);
  gTestD.agregarArista(1, 7, 11);
  gTestD.agregarArista(2, 3, 7);
  gTestD.agregarArista(2, 8, 2);
  gTestD.agregarArista(2, 5, 4);
  gTestD.agregarArista(3, 4, 9);
  gTestD.agregarArista(3, 5, 14);
  gTestD.agregarArista(4, 5, 10);
  gTestD.agregarArista(5, 6, 2);
  gTestD.agregarArista(6, 7, 1);
  gTestD.agregarArista(6, 8, 6);
  gTestD.agregarArista(7, 8, 7);

  Dj prueba1(op.Dijkstra(gTestD, 0));
  printf("Nodos  Distacias\n");
  for(vector<double>::iterator it = prueba1.first.begin(); it != prueba1.first.end(); it++){
    printf("%d \t %lf\n",it-prueba1.first.begin(),*it);
  }
  printf("Nodos  Ultimo nodo\n");
  for(vector<int>::iterator it = prueba1.second.begin(); it != prueba1.second.end(); it++){
    printf("%d \t %d\n",it-prueba1.second.begin(),*it);
  }

  /* test dijkstra ejemplo 2*/
  printf("Test Dijkstra ejemplo 2\n");
  Nodo<int> n1(1);
  Nodo<int> n2(2);
  Nodo<int> n3(3);
  Nodo<int> n4(4);
  Nodo<int> n5(5);
  Nodo<int> n6(6);
  Nodo<int> n7(7);
  Nodo<int> n8(8);
  GrafoPonderado<int> gTest3(false);
  gTest3.agregarNodo(n1);
  gTest3.agregarNodo(n2);
  gTest3.agregarNodo(n3);
  gTest3.agregarNodo(n4);
  gTest3.agregarNodo(n5);
  gTest3.agregarNodo(n6);
  gTest3.agregarNodo(n7);
  gTest3.agregarNodo(n8);
  printf("Numero de nodos: %d\nNumero de aristas: %d\n", gTest3.obtenerNumerosNodos(), gTest3.obtenerNumerosAristas());
  
  gTest3.agregarArista(0, 1, 16);
  gTest3.agregarArista(1, 2, 2);
  gTest3.agregarArista(2, 3, 4);
  gTest3.agregarArista(3, 0, 5);
  gTest3.agregarArista(0, 2, 10);
  gTest3.agregarArista(1, 6, 6);
  gTest3.agregarArista(6, 5, 8);
  gTest3.agregarArista(5, 2, 12);
  gTest3.agregarArista(5, 1, 4);
  gTest3.agregarArista(5, 4, 3);
  gTest3.agregarArista(3, 4, 15);
  gTest3.agregarArista(4, 2, 10);
  gTest3.agregarArista(4, 7, 5);
  gTest3.agregarArista(5, 7, 16);
  gTest3.agregarArista(6, 7, 7);

  Dj prueba2(op.Dijkstra(gTest3, 0)); 
  return 0;
}
