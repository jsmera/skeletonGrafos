#include "graph.h"
#include "graphSearch.h"
#include <iostream>
#include <cstdio>

using namespace std;

int main() {
  OperacionesGrafo<int> op;
  /* Test DFS BFS */
  Nodo<int> nb1(1); // 0
  Nodo<int> nb2(2); // 1
  Nodo<int> nb3(3); // 2
  Nodo<int> nb4(4); // 3
  Nodo<int> nb5(5); // 4
  Nodo<int> nb6(6); // 5
  Grafo<int> gTestBFS (false);
  gTestBFS.agregarNodo(nb1);
  gTestBFS.agregarNodo(nb2);
  gTestBFS.agregarNodo(nb3);
  gTestBFS.agregarNodo(nb4);
  gTestBFS.agregarNodo(nb5);
  gTestBFS.agregarNodo(nb6);
  
  gTestBFS.agregarArista(0, 1);
  gTestBFS.agregarArista(0, 4);
  gTestBFS.agregarArista(1, 2);
  gTestBFS.agregarArista(4, 1);
  gTestBFS.agregarArista(2, 3);
  gTestBFS.agregarArista(3, 4);
  gTestBFS.agregarArista(5, 3);

  op.BFS(gTestBFS, 4);
  op.DFS(gTestBFS, 0);

 
  return 0;
}
