#include "graph.h"
#include "graphSearch.h"
#include <iostream>
#include <cstdio>

using namespace std;

int main() {
  OperacionesGrafo<int> op;
  //Test grafo dirigido
  printf("Test grafo dirigido\n");

  Nodo<int> n1(1);
  Nodo<int> n2(2);
  Nodo<int> n3(3);
  Nodo<int> n4(4);
  Nodo<int> n5(5);
  Grafo<int> gTest2(true);
  gTest2.agregarNodo(n1);
  gTest2.agregarNodo(n2);
  gTest2.agregarNodo(n3);
  gTest2.agregarNodo(n4);
  gTest2.agregarNodo(n5);
  printf("Numero de nodos: %d\nNumero de aristas: %d\n", gTest2.obtenerNumerosNodos(), gTest2.obtenerNumerosAristas());

  gTest2.agregarArista(0, 2);
  gTest2.agregarArista(1, 0);
  gTest2.agregarArista(2, 1);
  gTest2.agregarArista(1, 4);
  gTest2.agregarArista(2, 3);
  gTest2.agregarArista(3, 1);
  gTest2.agregarArista(4, 2);
  gTest2.agregarArista(3, 4);
  printf("Numero de nodos: %d\nNumero de aristas: %d\n", gTest2.obtenerNumerosNodos(), gTest2.obtenerNumerosAristas());

  list_edge l3 = gTest2.obtenerListaAdyacencia(3);
  for (list_edge::iterator it = l3.begin(); it != l3.end(); it++) {
    printf("%d -- 0 --> %d \n", 3, it->first);
  }
  list_edge l1 = gTest2.obtenerListaAdyacencia(1);
  for (list_edge::iterator it = l1.begin(); it != l1.end(); it++) {
    printf("%d -- 0 --> %d \n", 1, it->first);
  }

  gTest2.eliminarArista(1, 0);
  cout << "Numero de aristas " << gTest2.obtenerNumerosAristas() << endl;
  cout << "Lista de abyaciencia con nodo 1" << endl;
  list_edge l2 = gTest2.obtenerListaAdyacencia(1);
  for (list_edge::iterator it = l2.begin(); it != l2.end(); it++) {
    cout << "1 --" << it->second << "--> " << it->first << endl;
  }
  
  //esta bien no deberia eliminarlo
  gTest2.eliminarArista(2, 0);
  cout << "Numero de aristas " << gTest2.obtenerNumerosAristas() << endl;
  cout << "Lista de abyaciencia con nodo 2" << endl;
  l2 = gTest2.obtenerListaAdyacencia(2);
  for (list_edge::iterator it = l2.begin(); it != l2.end(); it++) {
    cout << "2 --" << it->second << "--> " << it->first << endl;
  }

  gTest2.eliminarNodo(0);
  cout << "Numero de nodos " << gTest2.obtenerNumerosNodos() << endl;
  gTest2.eliminarArista(0, 2);
  gTest2.eliminarNodo(0);
  cout << "Numero de nodos " << gTest2.obtenerNumerosNodos() << endl;
  
  
  return 0;
}
