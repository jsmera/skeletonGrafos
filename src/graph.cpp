#include "graph.h"

void deleteEdge(list_edge& l, int form) {
  for (list_edge::iterator it = l.begin(); it != l.end(); it++) {
    //form es el nodo que va a buscar en la lista de adyacencia
    if(it->first == form) {
      l.erase(it);
      break;
    }
  }
}

bool nodeInListEdge(list_edge& l, int node) {
  for (list_edge::iterator it = l.begin(); it != l.end(); it++) {
    if(it->first == node) {
      return true;
    }
  }
  return false;
}