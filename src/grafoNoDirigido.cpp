#include "graph.h"
#include "graphSearch.h"
#include <iostream>
#include <cstdio>

using namespace std;

int main() {
  OperacionesGrafo<int> op;
  /* Test grafo no dirigido */
  Nodo<char> n1('H');
  Nodo<char> n2('A');
  Nodo<char> n3('W');
  Nodo<char> n4('F');
  Nodo<char> n5('Y');
  Nodo<char> n6('X');

  Grafo<char> gTest(false);
  gTest.agregarNodo(n1);
  gTest.agregarNodo(n2);
  gTest.agregarNodo(n3);
  gTest.agregarNodo(n4);
  gTest.agregarNodo(n5);
  gTest.agregarNodo(n6);

  cout << "Numero de nodos " << gTest.obtenerNumerosNodos() << endl;
  cout << "Numero de aristas " << gTest.obtenerNumerosAristas() << endl;

  gTest.agregarArista(0,1);
  gTest.agregarArista(1,3);
  gTest.agregarArista(1,2);
  gTest.agregarArista(2,5);
  gTest.agregarArista(5,4);
  gTest.agregarArista(2,4);
  // No deberia agregarlo
  gTest.agregarArista(2,6);

  cout << "Numero de nodos " << gTest.obtenerNumerosNodos() << endl;
  cout << "Numero de aristas " << gTest.obtenerNumerosAristas() << endl;

  cout << "Lista de abyaciencia con nodo 2" << endl;
  list_edge l2 = gTest.obtenerListaAdyacencia(2);
  for (list_edge::iterator it = l2.begin(); it != l2.end(); it++) {
    cout << "2 --" << it->second << "--> " << it->first << endl;
  }

  cout << "Eliminando arista de nodo 2 con nodo 4...." << endl;
  gTest.eliminarArista(2, 4);
  cout << "Numero de aristas " << gTest.obtenerNumerosAristas() << endl;
  cout << "Lista de abyaciencia con nodo 2" << endl;
  l2 = gTest.obtenerListaAdyacencia(2);
  for (list_edge::iterator it = l2.begin(); it != l2.end(); it++) {
    cout << "2 --" << it->second << "--> " << it->first << endl;
  }

  gTest.eliminarNodo(2);
  cout << "Numero de nodos " << gTest.obtenerNumerosNodos() << endl;
  gTest.eliminarArista(4, 5);
  gTest.eliminarNodo(4);
  cout << "Numero de nodos " << gTest.obtenerNumerosNodos() << endl;
  
  return 0;
}
