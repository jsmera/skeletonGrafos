#ifndef __GRAPH_H
#define __GRAPH_H

#include <vector>
#include <utility>
#include <list>
#include <iostream>

using namespace std;

template <class T>
class Nodo {
  public:
    T data;
    Nodo(T init) {
      data = init;
    }
};

using adjacency = vector< list< pair<int, double> > >;
using list_edge = list< pair<int, double> >;
template <class T>
using nuddles = vector< Nodo<T> >;

/** Miscellaneous **/

/**
deleteEdge
Parametros:
  list_edge& l: Una lista de adyacencia.
  int form: Identificador de un nodo.
Descripcion:
  Elimina de la lista de adyacencia dada un nodo dado.
Retorno: Vacio.
**/
void deleteEdge(list_edge& l, int form);

/**
nodeInListEdge
Parametros:
  list_edge& l: Una lista de adyacencia.
  int node: Identificador de un nodo.
Descripcion:
  Dada una de lista de adyacencia dada, devuelve si un nodo dado
  esta en la lista o no.
Retorno: Booleano.
**/
bool nodeInListEdge(list_edge& l, int node);
/*******************/

template <class T>
class Grafo {
  protected:
    adjacency core;
    nuddles<T> nodes;
    bool isDirected;
    int N_nodos;
    int N_aristas;
  
  public:
    Grafo(bool isDirected);
    Grafo(nuddles<T>& V, adjacency& E, bool isDirected);

    T getNode(int a);
    void showNodes();

    void agregarNodo ( Nodo<T>& n );
    void agregarArista (int a, int b);
    void eliminarNodo (int a);
    void eliminarArista (int a, int b);
    list_edge obtenerListaAdyacencia(int a);
    int obtenerNumerosNodos ();
    int obtenerNumerosAristas ();

    ~Grafo();
};

/**
Nombre: constructor vacio.
Parametros:
  bool isDirected: Este booleano representa si el grafo se quiere crear un grafo
  dirigido o no.
Retorno:
  Vacio.
**/
template<class T>
Grafo<T>::Grafo(bool isDirected) {
  this->isDirected = isDirected;
  N_nodos = 0;
  N_aristas = 0;
}

/**
Nombre: constructor no vacio.
Parametros:
  nuddles<T>& V: El conjunto de nodos con el cual se desea iniciar el grafo.
  adjacency& E: La lista de adyacencia con la cual se desea iniciar el grafo.
  bool isDirected: Este booleano representa si el grafo se quiere crear un grafo
  dirigido o no.
Retorno:
  Vacio.
**/
template<class T>
Grafo<T>::Grafo(nuddles<T>& V, adjacency& E, bool isDirected) {
  nodes = V;
  core = E;
  this->isDirected = isDirected;
  N_nodos = 0;
  N_aristas = 0;
}

/**
getNode
Parametros:
  int a: Identificador del nodo en el grafo.
Pre condiciones: El identificador debe de estar en el rango de
  identificadores del grafo.
Retorno:
  El dato que esta almacenado en el nodo.
**/
template<class T>
T Grafo<T>::getNode(int a) {
  if (a >= 0 && a < N_nodos) return nodes[a].data;
}

/**
showNodes
Descripcion:
  Imprime los nodos con su recpectivo identificador
Retorno:
  Vacio.
**/
template<class T>
void Grafo<T>::showNodes() {
  cout << "ID  Nodo" << endl;
  for(int i = 0; i < N_nodos; i++) {
    cout << i << "  " << nodes[i].data << endl;
  }
}

/**
Nombre: agregarNodo.
Parametros:
  Nodo<T>& n: Un nodo del mismo tipo de dato del grafo
    el cual se quiere agregar.
Retorno:
  Vacio.
Descripcion: Agrega el nodo al grafo, lo agrega al vector de nodos
  del grafo, y crea una lista de adyacencia vacia para dicho nodo
  tambien es agregada a la lista general de adyacencia del grafo, core.
**/
template <class T>
void Grafo<T>::agregarNodo(Nodo<T>& n){
  nodes.push_back(n);
  list_edge lista_n;
  core.push_back(lista_n);
  N_nodos++;
};

/**
agregarArista
Parametros:
  int a: Identificador del nodo partida de la arista a agregar. 
  int b: Identificador del nodo llegada de la arista a agregar.
Pre condiciones:
  Los identificadores debe de estar en el rango de
  identificadores del grafo y la arista a, b no puede existir.
Retorno:
  Vacio.
Descripcion:
  Crea la pareja <b, 0> en la lista de adyacencia de a y <a, 0> para
  la lista de b, en caso de que el grafo sea dirigido solo añadira <b, 0>
**/
template <class T>
void Grafo<T>::agregarArista(int a, int b) {
   if ( (a >= 0 && a < N_nodos) && (b >= 0 && b < N_nodos) ) {
    if (!nodeInListEdge(core[a], b)) {
      if (!isDirected) core[b].push_back(pair<int, double> (a, 0));
      core[a].push_back(pair<int, double> (b, 0));
      N_aristas++;
    }
  }
}

/**
eliminarArista
Parametros:
  int a: Identificador del nodo partida de la arista a agregar. 
  int b: Identificador del nodo llegada de la arista a agregar.
Pre condiciones:
  Los identificadores debe de estar en el rango de
  identificadores del grafo y la arista (a, b) debe de existir.
Retorno:
  Vacio.
Descripcion:
  Elimina la arista <b, 0> de la lista de adyacenca para a y <a, 0>
  de la lista de adyacencia para b, en caso de ser dirigo solo hara
  a primera accion.
**/
template <class T>
void Grafo<T>::eliminarArista(int a, int b) {
  if ( (a >= 0 && a < N_nodos) && (b >= 0 && b < N_nodos) ) {
    if (nodeInListEdge(core[a], b)) {
      if (!isDirected) deleteEdge(core[b], a);
      deleteEdge(core[a], b);
      N_aristas--;
    }
  }
}

/**
Nombre: eliminarNodo.
Parametros:
  int a: Identificador del nodo.
Post condiciones:
  Devuelve la cantidad de nodos-1, la nueva lista de adyacencia ya sin el nodo que se elimino
  y los id de los nodos que estaban por delante toman el nuevo valor.
Descripcion: Dado un nodo este se comprueba que no tenga ninguna arista hacia afuera como hacia 
  adentro. Dado el caso que el grafo no fuera dirigido miro si el vector con la lista de adyacencia
  miro si es vacia la lista en esa posicion si es vacia elimino.
  si es dirigido recorro el vector de adyacencia en caso de que este vacio puedo eliminar el nodo.
**/
template<class T>
void Grafo<T>::eliminarNodo(int a){
  int count=0;
  if (a >= 0 && a < N_nodos){
    if(isDirected){
      for(adjacency::iterator it=core.begin(); it!=core.end(); it++){
        for(list_edge::iterator it2=it->begin(); it2!=it->end();it2++){
          if(it2->first==a){
            count++;
          }
        }
      }
      if(count==0 && core[a].empty()){
        core.erase(core.begin()+a);
        nodes.erase(nodes.begin()+a);
        N_nodos--;
      }
    }
    else{
      if(core[a].empty()){
        core.erase(core.begin()+a);
        nodes.erase(nodes.begin()+a);
        N_nodos--;
     }  
    }
  }
}

/**
Nombre: obtenerListaAdyacencia.
Parametros:
  int a: a es el identificador del nodo.
Post condiciones:
  Devuelve la lista de adyacencia 
Descripcion: Primero se comprueba que a sea < al numero de nodos que hay, para
  verificar que no estamos buscando sobr eun nodo que no existe, y despues se
  accede al vector de adyacencia se le da el identificador del nodo y este accede
  a la lista de adyacencia del nodo respectivo. 
**/
template<class T>
list_edge Grafo<T>::obtenerListaAdyacencia(int a) {
  if (a >= 0 && a < N_nodos) return core[a];
}

/**
Nombre: obtenerNumerosNodos.
Retorno:
  Devuelve el numero de nodos.
Descripcion: Se tiene una variable que cada vez que se crea o se elimina un nodo, 
  esta aumenta o disminuye segun sea la situacion.
**/
template<class T> 
int Grafo<T>::obtenerNumerosNodos(){
  return N_nodos;
}

/**
Nombre: obtenerNumerosAristas.
Retorno:
  Devuelve el numero de aristas
Descripcion: Se tiene una variable que cada vez que se crea o se elimina una arista, 
  esta aumenta o disminuye segun sea la situacion.
**/
template<class T>
int Grafo<T>::obtenerNumerosAristas(){
  return N_aristas;
}

template<class T>
Grafo<T>::~Grafo() {}

template <class T>
class GrafoPonderado : public Grafo<T> {
  public:
    GrafoPonderado(bool);
    GrafoPonderado(nuddles<T>& V, adjacency& E, bool isDirected);

    void agregarArista (int a, int b, double weigth);
    void modificarArista (int a, int b, double weigth);

    ~GrafoPonderado();
};

template<class T>
GrafoPonderado<T>::GrafoPonderado(bool isDirected) : Grafo<int>(isDirected){
}

template<class T>
GrafoPonderado<T>::GrafoPonderado(nuddles<T>& V, adjacency& E, bool isDirected) : Grafo<int>(V, E, isDirected) {
}

/**
agregarArista
Parametros:
  int a: Identificador del nodo partida de la arista a agregar. 
  int b: Identificador del nodo llegada de la arista a agregar.
  double weight
Pre condiciones:
  Los identificadores debe de estar en el rango de
  identificadores del grafo y la arista a, b no puede existir.
Retorno:
  Vacio.
Descripcion:
  Crea la pareja <b, weight> en la lista de adyacencia de a y 
  <a, weight> para la lista de b, en caso de que el grafo sea
  dirigido solo añadira <b, weight>.
**/
template<class T>
void GrafoPonderado<T>::agregarArista(int a, int b, double weight){
  if(a < this->N_nodos && b < this->N_nodos){
    if(!nodeInListEdge(this->core[a], b)){
      if(!this->isDirected){
        this->core[b].push_back(pair<int,double>(a,weight));
      }
      this->core[a].push_back(pair<int,double>(b,weight));
      this->N_aristas++;
    }   
  }
}

/**
agregarArista
Parametros:
  int a: Identificador del nodo partida de la arista a agregar. 
  int b: Identificador del nodo llegada de la arista a agregar.
  double weight
Pre condiciones:
  Los identificadores debe de estar en el rango de
  identificadores del grafo y la arista a, b no exista.
Retorno:
  Vacio.
Descripcion:
  Modifica la pareja <b, weight> en la lista de adyacencia de a y
  <a, weight> para la lista de b, en caso de que el grafo sea
  dirigido solo modificara <b, weight>.
**/
template<class T>
void GrafoPonderado<T>::modificarArista(int a, int b, double weigth) {
  for(list_edge::iterator it = this->core[a].begin(); it != this->core[a].end(); it++) {
    it->second = weigth;
  }
}

template<class T>
GrafoPonderado<T>::~GrafoPonderado() {}

#endif
