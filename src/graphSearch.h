#ifndef __GRAPHSEARCH_H
#define __GRAPHSEARCH_H
#define Inf numeric_limits<double>::infinity()

#include <iostream>
#include <stack>
#include <queue>
#include <vector>
#include <limits>
#include <utility>
#include "graph.h"

using namespace std;
typedef pair< vector<double>,vector<int> > Dj;
typedef pair<int, double> edge;
class Order {
  public:
    bool operator() (edge& e1, edge& e2) {
      return e1.second < e2.second;
    }
};
typedef priority_queue<edge, vector<edge>,  Order > unvisited;

template<class T>
class OperacionesGrafo{
  public:
    //constructor
    OperacionesGrafo();

    void DFS(Grafo<T>&, int);
    void BFS(Grafo<T>&, int);
    Dj Dijkstra(GrafoPonderado<T>&, int);

    ~OperacionesGrafo();

};
template<class T>
OperacionesGrafo<T>::OperacionesGrafo(){
}

template<class T>
OperacionesGrafo<T>::~OperacionesGrafo(){
}

//dfs depht first search
template<class T>
void OperacionesGrafo<T> :: DFS (Grafo<T>& grafo1, int a){
  stack<int> recorrido;
  vector<bool> visit;
  for (int i = 0; i < grafo1.obtenerNumerosNodos(); i++) {
    visit.push_back(false);
  }
  recorrido.push(a);
  while(!recorrido.empty()){
    int nodo = recorrido.top();
    recorrido.pop();
    list_edge l1 = grafo1.obtenerListaAdyacencia(nodo);
    for (list_edge::iterator it = l1.begin(); it != l1.end(); it++) {
      int temp = it->first;
      if(visit[temp])
        continue;
      visit[temp] = true;
      cout<<temp<<", ";
      recorrido.push(it->first);
    }
  }
  cout<<endl;
}

template<class T>
void OperacionesGrafo<T>::BFS(Grafo<T>& G, int raiz) {
  queue<int> Q;
  Q.push(raiz);
  vector<bool> visited(G.obtenerNumerosNodos(), false);
  visited[raiz] = true;

  while( !Q.empty() ){
    int temp = Q.front();
    Q.pop();
    cout << temp << ", ";
    list_edge adjacent = G.obtenerListaAdyacencia(temp);
    for (list_edge::iterator it = adjacent.begin(); it != adjacent.end(); it++) {
      if (!visited[it->first]) {
        visited[it->first] = true;
        Q.push(it->first);
      }
    }
  }
  cout << endl;
}

template<class T>
Dj OperacionesGrafo<T>::Dijkstra(GrafoPonderado<T>& G, int origin) {
  vector<double> distances(G.obtenerNumerosNodos(), Inf);
  vector<int> path(G.obtenerNumerosNodos(), 0);
  path[origin] = origin;
  distances[origin] = 0;
  unvisited Q;
  Q.push(edge(origin, 0));

  while ( !Q.empty() ) {
    int nodeTemp = Q.top().first;
    Q.pop();
    //nodeTemp es el padre
    //nodeTo es el hijo
    list_edge l1 = G.obtenerListaAdyacencia(nodeTemp);
    for (list_edge::iterator it = l1.begin(); it != l1.end(); it++) {
      int nodeTo = it->first;
      int weigth = it->second;

      if (distances[nodeTo]  > distances[nodeTemp] + weigth) {
        path[nodeTo] = nodeTemp;
        distances[nodeTo] = distances[nodeTemp] + weigth;
        Q.push(edge(nodeTo, distances[nodeTo]));
      }
    }
  }

  return Dj(distances, path);
}

#endif
