#include "graph.h"
#include "graphSearch.h"
#include <iostream>
#include <cstdio>

using namespace std;

int main() {
  OperacionesGrafo<int> op;
  /* test grafo ponderado*/
  printf("Test grafo ponderado\n");
  Nodo<int> n1(1);
  Nodo<int> n2(2);
  Nodo<int> n3(3);
  Nodo<int> n4(4);
  Nodo<int> n5(5);
  GrafoPonderado<int> gTest3(true);
  gTest3.agregarNodo(n1);
  gTest3.agregarNodo(n2);
  gTest3.agregarNodo(n3);
  gTest3.agregarNodo(n4);
  gTest3.agregarNodo(n5);
  printf("Numero de nodos: %d\nNumero de aristas: %d\n", gTest3.obtenerNumerosNodos(), gTest3.obtenerNumerosAristas());
  
  gTest3.agregarArista(0, 2, 20.0);
  gTest3.agregarArista(1, 0, 10.5);
  gTest3.agregarArista(2, 1, 30.6);
  gTest3.agregarArista(1, 4, 5.6);
  gTest3.agregarArista(2, 3, 6.0);
  gTest3.agregarArista(3, 1, 41.6);
  gTest3.agregarArista(4, 2, 28.4);
  gTest3.agregarArista(3, 4, 15.6);
  printf("Numero de nodos: %d\nNumero de aristas: %d\n", gTest3.obtenerNumerosNodos(), gTest3.obtenerNumerosAristas());
  
  list_edge l3 = gTest3.obtenerListaAdyacencia(3);
  for (list_edge::iterator it = l3.begin(); it != l3.end(); it++) {
    printf("%d -- 0 --> %d -- 0 -->%f \n", 3, it->first, it->second);
  }
  
  gTest3.modificarArista(0, 2, 24.5); 
  list_edge l2 = gTest3.obtenerListaAdyacencia(0);
  for (list_edge::iterator it = l2.begin(); it != l2.end(); it++) {
    printf("%d -- 0 --> %d -- 0 -->%f \n", 0, it->first, it->second);
  }
  
  
  return 0;
}
